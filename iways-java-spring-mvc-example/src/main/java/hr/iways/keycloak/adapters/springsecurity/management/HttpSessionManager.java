package hr.iways.keycloak.adapters.springsecurity.management;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.keycloak.adapters.spi.UserSessionManagement;
import org.keycloak.adapters.springsecurity.management.LocalSessionManagementStrategy;
import org.keycloak.adapters.springsecurity.management.SessionManagementStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.security.web.session.HttpSessionCreatedEvent;
import org.springframework.security.web.session.HttpSessionDestroyedEvent;

public class HttpSessionManager implements UserSessionManagement, ApplicationListener<ApplicationEvent> {

	private static final Logger log = LoggerFactory.getLogger(HttpSessionManager.class);
	private SessionManagementStrategy sessions = new LocalSessionManagementStrategy();

	@Override
	public void onApplicationEvent(ApplicationEvent event) {
		if (event instanceof HttpSessionCreatedEvent) {
			HttpSessionCreatedEvent e = (HttpSessionCreatedEvent)event;
			HttpSession session = e.getSession();
			log.debug("Session created: {}", session.getId());
			sessions.store(session);
		} else if (event instanceof HttpSessionDestroyedEvent) {
			HttpSessionDestroyedEvent e = (HttpSessionDestroyedEvent)event;
			HttpSession session = e.getSession();
			sessions.remove(session.getId());
			log.debug("Session destroyed: {}", session.getId());

		}
	}

	@Override
	public void logoutAll() {
		log.info("Received request to log out all users.");
		for (HttpSession session : sessions.getAll()) {
			session.invalidate();
		}
		sessions.clear();
	}

	@Override
	public void logoutHttpSessions(List<String> ids) {
		log.info("Received request to log out {} session(s): {}", ids.size(), ids);
		for (String id : ids) {
			HttpSession session = sessions.remove(id);
			if (session != null) {
				session.invalidate();
			}
		}
	}
}