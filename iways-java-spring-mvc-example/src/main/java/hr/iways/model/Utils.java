package hr.iways.model;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utils {

	private static final Logger _log = LoggerFactory.getLogger(Utils.class);

	public static AccessToken getAccessToken(HttpServletRequest request) {
		try {
			KeycloakAuthenticationToken kat = request.getUserPrincipal() != null ? (KeycloakAuthenticationToken) request.getUserPrincipal() : null;

			if (kat == null) {
				return null;
			}

			AccessToken at = kat.getAccount()
					.getKeycloakSecurityContext()
					.getToken();
			return at;
		} catch (Exception ex) {
			_log.error("Error happened while retrieving access token!", ex);
		}

		return null;
	}

	public static UserDetails getUserDetails(HttpServletRequest request) {
		UserDetails ud = new UserDetails();

		AccessToken at = getAccessToken(request);
		if (at != null) {
			ud.setEmail(at.getEmail());
			ud.setFirstName(at.getGivenName());
			ud.setLastName(at.getFamilyName());
			ud.setUserCustomAttribute(getClaimFromAccessToken(at, "userCustomAttribute"));
		}

		HttpSession session = request.getSession(false);
		ud.setSessionId(session != null ? session.getId() : null);

		return ud;
	}

	private static String getClaimFromAccessToken(AccessToken at, String claimKey) {
		if (at == null || claimKey == null) {
			return null;
		}

		Map<String, Object> claims = at.getOtherClaims();
		if (claims == null) {
			return null;
		}

		Object valueObj = claims.get(claimKey);
		if (valueObj instanceof String) {
			return (String) valueObj;
		} else {
			return String.valueOf(valueObj);
		}
	}
}
