package hr.iways.model;

public final class UserDetails {

	private String firstName;
	private String lastName;
	private String email;
	private String sessionId;
	private String userCustomAttribute;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getUserCustomAttribute() {
		return userCustomAttribute;
	}

	public void setUserCustomAttribute(String userCustomAttribute) {
		this.userCustomAttribute = userCustomAttribute;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserDetails [firstName=").append(firstName).append(", lastName=").append(lastName)
		.append(", email=").append(email).append(", sessionId=").append(sessionId)
		.append(", userCustomAttribute=").append(userCustomAttribute).append("]");
		return builder.toString();
	}
}