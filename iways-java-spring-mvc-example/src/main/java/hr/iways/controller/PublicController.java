package hr.iways.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import hr.iways.model.Utils;

@Controller
public class PublicController {

	@RequestMapping(value="/", method = RequestMethod.GET)
	public ModelAndView showIndexPage(final HttpServletRequest request, final HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("index");
		mv.addObject("userDetails", Utils.getUserDetails(request));

		return mv;
	}

	@RequestMapping(value="public-page", method = RequestMethod.GET)
	public ModelAndView showPublicPage(final HttpServletRequest request, final HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("public_page");
		mv.addObject("userDetails", Utils.getUserDetails(request));

		return mv;
	}
}