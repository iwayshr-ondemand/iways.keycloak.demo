package hr.iways.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import hr.iways.model.Utils;

@Controller
public class PrivateController {

	@RequestMapping(value="private-page", method = RequestMethod.GET)
	public ModelAndView showPrivatePage(final HttpServletRequest request, final HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("private_page");
		mv.addObject("userDetails", Utils.getUserDetails(request));

		return mv;
	}

	@RequestMapping(value="private-page-admin", method = RequestMethod.GET)
	public ModelAndView showPrivatePageAdmin(final HttpServletRequest request, final HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("private_page_admin");
		mv.addObject("userDetails", Utils.getUserDetails(request));

		return mv;
	}

	@RequestMapping(value="login", method = RequestMethod.GET)
	public ModelAndView login(final HttpServletRequest request, final HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("login");
		mv.addObject("userDetails", Utils.getUserDetails(request));

		return mv;
	}
}
