<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<spring:url value="/" htmlEscape="true" var="backUrl" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Public page</title>
</head>
<body>
	<h1>I'm public page</h1>
	<%@ include file="user_details.jsp"%>
	<br>
	<p><a href="${backUrl}">Back</a></p>
</body>
</html>