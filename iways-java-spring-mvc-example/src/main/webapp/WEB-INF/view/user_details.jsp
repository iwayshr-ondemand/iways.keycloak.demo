<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${not empty userDetails}">
	<c:if test="${not empty userDetails.email}">
		<p>Hello ${userDetails.firstName} ${userDetails.lastName} (${userDetails.email})</p>
	</c:if>
	<c:if test="${not empty userDetails.sessionId}">
		<p>Session id: ${userDetails.sessionId}</p>
	</c:if>
	<c:if test="${not empty userDetails.userCustomAttribute}">
		<p>Custom attribute value: ${userDetails.userCustomAttribute}</p>
	</c:if>
</c:if>
