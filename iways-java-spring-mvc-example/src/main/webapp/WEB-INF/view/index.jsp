<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<spring:url value="/login" htmlEscape="true" var="loginUrl" />
<spring:url value="/private-page" htmlEscape="true" var="privateUrl" />
<spring:url value="/private-page-admin" htmlEscape="true" var="privateAdminUrl" />
<spring:url value="/public-page" htmlEscape="true" var="publicUrl" />
<spring:url value="/sso/logout" htmlEscape="true" var="sloUrl" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Spring Boot starting page</title>
</head>
<body>
	<h1>Starting page</h1>
	<%@ include file="user_details.jsp"%>
	<br>
	<p>Visit private page <a href="${privateUrl}">here</a></p>
	<p>Visit private admin page <a href="${privateAdminUrl}">here</a></p>
	<p>Visit public page <a href="${publicUrl}">here</a></p>
	<br>
	<br>
	<p><a href="${sloUrl}">Logout</a></p>
</body>
</html>