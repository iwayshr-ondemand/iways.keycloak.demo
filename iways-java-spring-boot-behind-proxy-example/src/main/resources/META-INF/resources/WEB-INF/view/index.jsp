<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<spring:url value="/users" htmlEscape="true" var="usersUrl" />
<spring:url value="/admins" htmlEscape="true" var="adminsUrl" />
<spring:url value="/users/permit" htmlEscape="true" var="usersPermitUrl" />
<spring:url value="/users/deny" htmlEscape="true" var="usersDenyUrl" />
<spring:eval expression="@environment.getProperty('app.slo.url')" var="sloUrl" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Spring behind proxy demo app</title>
</head>
<body>
	<h1>Starting page</h1>
	<%@ include file="user_details.jsp"%>
	<br>
	<p>Visit private users page <a href="${usersUrl}">here</a></p>
	<p>Visit private admins page <a href="${adminsUrl}">here</a></p>
	<p>Visit users permit page <a href="${usersPermitUrl}">here</a></p>
	<p>Visit users deny page <a href="${usersDenyUrl}">here</a></p>
	<br>
	<br>
	<p><a href="${sloUrl}">Logout</a></p>
</body>
</html>