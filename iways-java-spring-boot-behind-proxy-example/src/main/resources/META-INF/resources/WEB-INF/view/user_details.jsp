<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${not empty userDetails}">
	<c:if test="${not empty userDetails.email}">
		<p>Hello ${userDetails.name} (${userDetails.email})</p>
	</c:if>
	<c:if test="${not empty userDetails.username}">
		<p>Username: ${userDetails.username}</p>
	</c:if>
	<c:if test="${not empty userDetails.subject}">
		<p>Subject: ${userDetails.subject}</p>
	</c:if>
	<c:if test="${not empty userDetails.accessToken}">
		<p>Access token: ${userDetails.accessToken}</p>
	</c:if>
</c:if>