package hr.iways.model;

import javax.servlet.http.HttpServletRequest;

public class Utils {
	
	private static final String KEYCLOAK_SUBJECT = "keycloak_subject";
	private static final String KEYCLOAK_EMAIL = "keycloak_email";
	private static final String KEYCLOAK_ACCESS_TOKEN = "keycloak_access_token";
	private static final String KEYCLOAK_NAME = "keycloak_name";
	private static final String KEYCLOAK_USERNAME = "keycloak_username";

	public static UserDetails getUserDetails(HttpServletRequest request) {
		UserDetails ud = new UserDetails();

		ud.setAccessToken(request.getHeader(KEYCLOAK_ACCESS_TOKEN));
		ud.setEmail(request.getHeader(KEYCLOAK_EMAIL));
		ud.setName(request.getHeader(KEYCLOAK_NAME));
		ud.setSubject(request.getHeader(KEYCLOAK_SUBJECT));
		ud.setUsername(request.getHeader(KEYCLOAK_USERNAME));

		return ud;
	}

}
