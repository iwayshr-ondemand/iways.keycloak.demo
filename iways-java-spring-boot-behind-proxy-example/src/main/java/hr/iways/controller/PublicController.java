package hr.iways.controller;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import hr.iways.model.Utils;

@Controller
public class PublicController {

	private static final Logger log = LoggerFactory.getLogger(PublicController.class);

	@RequestMapping(path="/", method = RequestMethod.GET)
	public ModelAndView index(final HttpServletRequest request, final HttpServletResponse response) {
		log.info("Requesting index");
		return new ModelAndView("index");
	}

	@RequestMapping(path="users", method = RequestMethod.GET)
	public ModelAndView users(final HttpServletRequest request, final HttpServletResponse response) {
		log.info("Requesting users");
		
		ModelAndView mv = new ModelAndView("users");
		mv.addObject("userDetails", Utils.getUserDetails(request));
		
		printAllFromRequest(request);
		return mv;
	}

	@RequestMapping(path="admins", method = RequestMethod.GET)
	public ModelAndView admins(final HttpServletRequest request, final HttpServletResponse response) {
		log.info("Requesting admins");
		
		ModelAndView mv = new ModelAndView("admins");
		mv.addObject("userDetails", Utils.getUserDetails(request));
		
		return mv;
	}

	@RequestMapping(path="users/permit", method = RequestMethod.GET)
	public ModelAndView usersPermit(final HttpServletRequest request, final HttpServletResponse response) {
		log.info("Requesting users/permit");
		
		ModelAndView mv = new ModelAndView("users_permit");
		mv.addObject("userDetails", Utils.getUserDetails(request));

		printAllFromRequest(request);
		return mv;
	}

	@RequestMapping(path="users/deny", method = RequestMethod.GET)
	public ModelAndView usersDeny(final HttpServletRequest request, final HttpServletResponse response) {
		log.info("Requesting users/deny");
		return new ModelAndView("users_deny");
	}

	@RequestMapping(path="unauthorized", method = RequestMethod.GET)
	public ModelAndView error(final HttpServletRequest request, final HttpServletResponse response) {
		log.info("Requesting unauthorized");
		printAllFromRequest(request);
		return new ModelAndView("unauthorized");
	}
	
	private static void printAllFromRequest(final HttpServletRequest request) {
		Enumeration<String> attrNames = request.getAttributeNames();
		if (attrNames != null) {
			log.debug("[IGOR] Printing attributes:");
			while (attrNames.hasMoreElements()) {
				String attrName = attrNames.nextElement();
				log.debug("[IGOR] " + attrName + ": <" + request.getAttribute(attrName) + ">");
			}
		}
		
		Enumeration<String> paramNames = request.getParameterNames();
		if (paramNames != null) {
			log.debug("[IGOR] Printing parameters:");
			while (paramNames.hasMoreElements()) {
				String paramName = paramNames.nextElement();
				log.debug("[IGOR] " + paramName + ": <" + request.getParameter(paramName) + ">");
			}
		}
				
		final Enumeration<String> headerNames = request.getHeaderNames();
		if (headerNames != null) {
			log.debug("[IGOR] Printing headers:");
			while (headerNames.hasMoreElements()) {
				final String headerName = headerNames.nextElement();
				log.debug("[IGOR] " + headerName + ": <" + request.getHeader(headerName) + ">");
			}
		}
	}
}