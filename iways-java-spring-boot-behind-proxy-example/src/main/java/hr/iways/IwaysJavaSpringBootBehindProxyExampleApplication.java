package hr.iways;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IwaysJavaSpringBootBehindProxyExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(IwaysJavaSpringBootBehindProxyExampleApplication.class, args);
	}
}
