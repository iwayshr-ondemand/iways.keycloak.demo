package hr.iways.api;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProtectedService {

	private static class ServiceResult {
		private String hardCoded1;
		private int hardCoded2;

		public ServiceResult() {
			super();
		}

		public ServiceResult(String hardCoded1, int hardCoded2) {
			super();
			this.hardCoded1 = hardCoded1;
			this.hardCoded2 = hardCoded2;
		}

		public String getHardCoded1() {
			return hardCoded1;
		}

		public void setHardCoded1(String hardCoded1) {
			this.hardCoded1 = hardCoded1;
		}

		public int getHardCoded2() {
			return hardCoded2;
		}

		public void setHardCoded2(int hardCoded2) {
			this.hardCoded2 = hardCoded2;
		}
	}

	private static final ServiceResult SR = new ServiceResult("Backend service created me :)", 9999);

	@CrossOrigin
	@RequestMapping(value = "/services/protected/hardcoded-result",
	method = {RequestMethod.GET},
	produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@PreAuthorize("hasAnyRole(['ROLE_USER','ROLE_ADMIN'])")
	public ServiceResult getHardcodedResult() {
		return SR;
	}
}