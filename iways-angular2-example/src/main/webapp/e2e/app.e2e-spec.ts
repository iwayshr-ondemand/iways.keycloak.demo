import { IwaysAngular2KeycloakDemoPage } from './app.po';

describe('iways-angular2-keycloak-demo App', function() {
  let page: IwaysAngular2KeycloakDemoPage;

  beforeEach(() => {
    page = new IwaysAngular2KeycloakDemoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
