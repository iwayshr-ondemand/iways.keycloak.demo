export const environment = {
  production: true,
  keycloakProperties: {
    "realm": "flive-dev-realm",
    "realm-public-key": "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmVR4Fs2tHHF/FfUo5BjSRaaic5nv85rFAZJ17Bon/axjjSwrZLLLbESvJWFol+XG6rM1V3ztn8j6x5nkbksAOI+3ydn7FRwKNUedaPy6h1I4etK9AyQCbn5is270TalOjjV75s4p6V91//utIHN4m8GcCcw5tbsJejPbtxGR6WKlOAqW5xLhRkFT0d2aBsyGCf8QXI09svHq5X0YAZCSLkgYDOen7KNGKI+hzvlZtUQ7SI9rd8LyvaJ6XfE3xFWDvMlN0lRBen3nfzG36iDVRtYzwUBXPyC/INcxwMwJnc6ozz/TgKpa/jYhhOnAavhwV+CbHKqzZdNJQFWzB2o0dwIDAQAB",
    "auth-server-url": "https://flive1.i-ways.hr/auth",
    "ssl-required": "external",
    "resource": "sso-demo-ang2-client-flane4",
    "clientId": "sso-demo-ang2-client-flane4",
    "public-client": true,
    "url": "https://flive1.i-ways.hr/auth"
  },
  keycloakLogoutRedirectUrl: "/realms/flive-dev-realm/protocol/openid-connect/logout?redirect_uri=https://flane4.i-ways.hr/ang2-keycloak/index.html",
  wsEndpointHardcodedResult: "https://flane5.i-ways.hr/keycloak-ws-service-app/services/protected/hardcoded-result"
};
