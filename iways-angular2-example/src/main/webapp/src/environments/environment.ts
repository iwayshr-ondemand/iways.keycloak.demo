// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
  production: false,
  keycloakProperties: {
    "realm": "sso-demo",
    "realm-public-key": "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkiN2BJqvEagqrNKrz2QCyw3fE2PdF0aMv6tHCw0Tica3MqfFKklgMq0Bw1pixnXWyTl32cBRZewfbGjFefIDikwv3EiMVadi61vxtDqV8SZuz/8n0lxVRhdIBUFNug76RX9WrigXTGNT18v7XHQ5VtdeelenCXhXzH+tzKrsU/kjZU98hrL4kt9x7okAlbwkIY6u/vPwV9uTUR1YsP32zOjnTEDPjIV9P6kxPYxFrvyaLLkqrRUDEyQjEO4CQrThkFUN5gH5xw7SFn+PBMf505P8//zJZBgyrSjgo3HsudxHHU/AubQxCcQMx7ShkvdYV7CMqFO9zDbKJNgkLBDzSwIDAQAB",
    "auth-server-url": "http://localhost:8180/auth",
    "ssl-required": "external",
    "resource": "sso-demo-ang2-client",
    "clientId": "sso-demo-ang2-client",
    "public-client": true,
    "url": "http://localhost:8180/auth"
  },
  keycloakLogoutRedirectUrl: "/realms/sso-demo/protocol/openid-connect/logout?redirect_uri=http://localhost:4200/ang2-keycloak/index.html",
  wsEndpointHardcodedResult: "http://localhost:8080/keycloak-ws-service-app/services/protected/hardcoded-result"
};