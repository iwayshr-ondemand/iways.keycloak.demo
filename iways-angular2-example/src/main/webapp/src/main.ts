import './polyfills.ts';

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { environment } from './environments/environment';
import { AppModule } from './app/';
import { KeycloakService } from "./app/keycloak.service";

if (environment.production) {
  enableProdMode();
}

//platformBrowserDynamic().bootstrapModule(AppModule);

/*
new Promise((resolve) => {
  console.log('preparing to load...')
  let node = document.createElement('script');
  node.src = environment.keycloakJsUrl;
  node.type = 'text/javascript';
  node.async = true;
  node.charset = 'utf-8';
  document.getElementsByTagName('head')[0].appendChild(node);
}).then(() => {
  console.log('Finished injecting JS!');
  KeycloakService.init()
    .then(() => {
      const platform = platformBrowserDynamic();
      platform.bootstrapModule(AppModule);
    })
    .catch((err) => {
      console.log(err);
      //window.location.reload()
    })
});
*/

KeycloakService.init()
  .then(() => {
    const platform = platformBrowserDynamic();
    platform.bootstrapModule(AppModule);
  })
  .catch((err) => {
    console.log(err);
    //window.location.reload()
  });