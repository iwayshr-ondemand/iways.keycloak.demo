import { Injectable } from "@angular/core";
import { environment } from "../environments/environment";
//import * as Keycloak from "keycloak-js";

declare var Keycloak: any;

@Injectable()
export class KeycloakService {
    static auth: any = {};

    static init(): Promise<any> {
        let keycloakAuth: any = new Keycloak(environment.keycloakProperties);
        KeycloakService.auth.loggedIn = false;

        return new Promise((resolve, reject) => {
            keycloakAuth.init({ onLoad: 'login-required' })
                .success(() => {
                    KeycloakService.auth.loggedIn = true;
                    KeycloakService.auth.authz = keycloakAuth;
                    KeycloakService.auth.logoutUrl = keycloakAuth.authServerUrl + environment.keycloakLogoutRedirectUrl;
                    resolve();
                })
                .error((err) => {
                    console.log(err)
                    reject();
                });
        });
    }

    logout() {
        console.log('*** LOGOUT');
        KeycloakService.auth.loggedIn = false;
        KeycloakService.auth.authz = null;

        window.location.href = KeycloakService.auth.logoutUrl;
    }

    getToken(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            if (KeycloakService.auth.authz.token) {
                KeycloakService.auth.authz.updateToken(5)
                    .success(() => {
                        resolve(<string>KeycloakService.auth.authz.token);
                    })
                    .error(() => {
                        reject('Failed to refresh token');
                    });
            }
        });
    }
}