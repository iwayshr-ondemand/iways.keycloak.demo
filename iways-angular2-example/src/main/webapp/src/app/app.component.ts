import { Component } from "@angular/core";
import { Http, Headers, RequestOptions, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import { KeycloakService } from "./keycloak.service";
import { ServiceResult } from "./service-result";
import { environment } from "../environments/environment";

@Component({
  selector: 'app-root',
  templateUrl: './app.template.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  serviceResult: ServiceResult = null;

  constructor(private http: Http, private kc: KeycloakService) { }

  logout() {
    this.kc.logout();
  }

  reloadData() {
    this.http.get(environment.wsEndpointHardcodedResult)
      .map(res => res.json())
      .subscribe(
      prods => { console.log(prods); this.serviceResult = prods; },
      error => console.log(error));
  }
}