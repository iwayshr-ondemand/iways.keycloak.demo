/**
 * Model representing a service result.
 */
export class ServiceResult {
    hardCoded1: string;
    hardCoded2: number;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}